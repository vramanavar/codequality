1.	Install "Microsoft.CodeAnalysis.FxCopAnalyzers" nuget package
2.	Expand the "Dependencies" from the project, select "Microsoft.CodeQuality.Analyzers" and set the individual rule's "Rule Set Severity" to "Error/Warning/Information/Default" or others.
	When the severity is overridden; a "<<ProjectName>>.ruleset" file is created in the project file
3.	To set code quality metrics thresholds; add a text file called "CodeMetricsConfig.txt" and mark it's "C# analyzer additional file" in "Action" property of that file
	a)	Cyclomatic Complexity is set to 8 as below
		CA1502: 8
	b)	Maintainability Index is set to 20 (Green Rating: 20-100 - Good Maintainability, Yellow Rating 10-19 - Moderately Maintainable, Red Rating 0-9 - Low Maintainability)
		CA1505: 20
4.	Add "Microsoft.CodeAnalysis.Metrics" package the project to generate metrics. Once added execute the following command to get the metrics report
	msbuild /t:Metrics /p:LEGACY_CODE_METRICS_MODE=true /p:MetricsOutputFile="Legacy.xml"
5.	SecurityCodeScan: Protects the code from common OWASP security threats
6.	In addition to these add Unit Test to the project